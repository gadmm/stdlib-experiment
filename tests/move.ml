let is_none x = max_int == (Obj.magic x : int)

let test1 x y =
  assert (not (is_none x));
  let z = x in
  assert (is_none (Obj.move x));
  assert (y == z)

let test2 x =
  test1 x x;
  assert (not (is_none x))

let _ = test2 (0,0) (* or any other *boxed* value *)
let _ = for i = 1 to 1000 do test2 i done

