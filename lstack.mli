(* GMM 2018. Original copyright retained: *)
(***********************************************************************)
(*                                                                     *)
(*                                OCaml                                *)
(*                                                                     *)
(*            Xavier Leroy, projet Cristal, INRIA Rocquencourt         *)
(*                                                                     *)
(*  Copyright 1996 Institut National de Recherche en Informatique et   *)
(*  en Automatique.  All rights reserved.  This file is distributed    *)
(*  under the terms of the GNU Library General Public License, with    *)
(*  the special exception on linking described in file ../LICENSE.     *)
(*                                                                     *)
(***********************************************************************)

(** Linear Last-in first-out stacks.

   This module implements stacks (LIFOs), with in-place modification.
*)

type +^a t : Affine
(** The type of stacks containing elements of type [^a]. *)

exception Empty
(** Raised when {!LStack.pop} or {!LStack.top} is applied to an empty
    stack. *)


val create : unit -> ^a t
(** Return a new stack, initially empty. *)

val swap : ^a t && ->> ^a t && -@mut> unit
(** Exchange the contents of two stacks *)

val take : ^a t && -@mut> ^a t
(** Take possession of the contents of the given stack. The argument
    is emptied in the process. *)

val push : ^a ->> ^a t && -@mut> unit
(** [push x s] adds the element [x] at the top of stack [s]. *)

val pop : ^a t && -@mut> ^a
(** [pop s] removes and returns the topmost element in stack [s],
    or raises [Empty] if the stack is empty. *)

val top : ^a t &&@r -@> ^a &&@r
(** [top s] returns the topmost element in stack [s],
    or raises [Empty] if the stack is empty. *)

val clear : ^a t && -@mut> unit
(** Discard all elements from a stack. *)

val copy : (^a : Copy@) t & -@> ^a t
(** Return a copy of the given stack. *)

val is_empty : ^a t & -@> bool
(** Return [true] if the given stack is empty, [false] otherwise. *)

val length : ^a t & -@> int
(** Return the number of elements in a stack. *)

val iter : <Seq@>(^a && ~>> unit) ->> ^a t && ~@> unit
(** [iter f s] applies [f] in turn to all elements of [s],
   from the element at the top of the stack to the element at the
   bottom of the stack. The stack itself is unchanged. *)

val contents : ^a t &&@r -@const> ^a list &&@r
