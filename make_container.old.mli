(** A trivial data structure holding only one element, following the
   functorial idiom. This is to demonstrate how to implement resource
   polymorphism with this idiom. *)

module type OrderedType =
  sig
    type t
    val compare: t & const ->> t & const -@>> int
  end
(** What we want to parametrize over *)

module type Singleton =
  sig
    type elt : Own@
    type t : <elt>
    (** Contains one element equal to itself. *)
    val singleton: elt -@>> t
    (** Creates a new singleton. Raises Invalid_argument if the
       argument is not equal to itself. *)
    val get: t -@>> elt
  end
(** The interface we want to implement. *)

module type PolySingleton =
  sig
    include Singleton
    module Borrow : Singleton with type elt = elt & and type t = t &
    module Const_borrow : Singleton with type elt = elt & const and type t = t & const
  end
(** How we make it resource-polymorphic. *)

module Make (Ord : OrderedType) : PolySingleton with type elt = Ord.t
