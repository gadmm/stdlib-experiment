WIP

TL;DR:

## 1. list.{org,ml,mli}

<https://gitlab.com/gadmm/stdlib-experiment/compare/ocaml...master#322ac4bcd5bb88fa8359da72a5898175d77ab302>

<https://gitlab.com/gadmm/stdlib-experiment/compare/ocaml...master#f8e8bea1077ada10c3237bd8b0a9c7f74cd041ab>

<https://gitlab.com/gadmm/stdlib-experiment/raw/master/list.org>

* We introduce a sequential polarity : a polarity for non-aliasable
  borrows, and describe the aliasing guarantees.

* We describe the diverse closure types.

* Our notion of resource polymorphism lets us extend the List module
  to resource-sensitive types to a full extent, and learn lessons in
  the process.

  * Having a type of static closures (CBPV arrow) is crucial for any
    realistic extension; in practice it is very close to ```->>```.

  * In the case of Own types, predicates and comparison functions
    require a universal quantification of a region, which we can
    implement with records.

  * We encapsulate the disjointness of regions using a remark of Baker,
    and achieve Currying for borrow types.

  * We observe that tail calls must be the norm; and survival of a
    resource during a last call the exception.

  * We refine the requirements for pattern-matching Owned values.

  * We analyse the impact of the “write-once, compile-twice” approach to
    runtime polarities


## 2. new/{rc,rust_mutex,zipper}.{org,ml,mli}

<https://gitlab.com/gadmm/stdlib-experiment/tree/master/new>

* We describe two modules that extends the ownership model using
  "unsafe" à la Rust: Rc and Rust_mutex.

* We deal with asynchronous exceptions by placing "new" and
  destructors inside critical sections for asynchronous exceptions.

* We give an example of application of resource polymorphism with the
  Zipper.


## 3. set.{org,ml,mli}

<https://gitlab.com/gadmm/stdlib-experiment/compare/ocaml...master#fac10dc955ec86d82ea670d0b879179840b058c9>

<https://gitlab.com/gadmm/stdlib-experiment/compare/ocaml...master#80b3f5a8d7b72d18214e149e074e5353d89d1e40>

<https://gitlab.com/gadmm/stdlib-experiment/raw/master/set.org>

* Set is functor-based rather than based on parametric
  polymorphism. We give a resource-polymorphic extension inspired by
  List and conclude that this requires generalising the structural
  functors ```&``` and ```const``` to a generic polarity cast indexed
  by a type or a reified polarity.

* We note that first-class modules have a polarity, and that functors,
  like functions, have to be annotated with effects and polarities.

## 3. lstack.{org,ml,mli}

<https://gitlab.com/gadmm/stdlib-experiment/raw/master/lstack.org>

* Need a new, distinct, mutable state that guarantees non-aliasing

* Remove ```const``` in favour of a better solution

* Think about ```^_weak```