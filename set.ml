(* GMM 2018. Original copyright retained: *)
(***********************************************************************)
(*                                                                     *)
(*                                OCaml                                *)
(*                                                                     *)
(*            Xavier Leroy, projet Cristal, INRIA Rocquencourt         *)
(*                                                                     *)
(*  Copyright 1996 Institut National de Recherche en Informatique et   *)
(*  en Automatique.  All rights reserved.  This file is distributed    *)
(*  under the terms of the GNU Library General Public License, with    *)
(*  the special exception on linking described in file ../LICENSE.     *)
(*                                                                     *)
(***********************************************************************)

(* Sets over ordered types *)

module type OrderedType =
  sig
    type t
    val compare: t ->> t -> int
  end

module type S =
  sig
    type elt
    type t
    val empty: t
    val is_empty: t ->> bool
    val mem: elt ->> t -> bool
    val add: elt ->> t -> t
    val singleton: elt ->> t
    val remove: elt ->> t -> t
    val union: t ->> t -> t
    val inter: t ->> t -> t
    val diff: t ->> t -> t
    val compare: t ->> t -> int
    val equal: t ->> t -> bool
    val subset: t ->> t -> bool
    val iter: (elt ~> unit) ->> t ~> unit
    val fold: (elt ->> 'a ~> 'a) ->> t ->> 'a ~> 'a
    val for_all: (elt ~> bool) ->> t ~> bool
    val exists: (elt ~> bool) ->> t ~> bool
    val filter: (elt ~> bool) ->> t ~> t
    val partition: (elt ~> bool) ->> t ~> t * t
    val cardinal: t ->> int
    val elements: t ->> elt list
    val min_elt: t ->> elt
    val max_elt: t ->> elt
    val choose: t ->> elt
    val split: elt ->> t -> t * bool * t
    val find: elt ->> t -> elt
    val of_list: elt list -> t
  end

module type OrderedTypePure =
  sig
    type +^a t
    val compare: _ t & const ->> _ t & const -@>> int
  end

module type SPoly =
  sig
    type +^a elt
    type +^a t
    (** The destructor of t destroys in order, from the smallest to
       the greatest element. *)
    val empty: _ t & const
    val make_empty : unit ->> ^a t
    val is_empty: _ t & const -@>> bool
    val mem: ^a elt & const ->> ^a t & const -@>> bool
    val add: ^a elt ->> ^a t -@>> ^a t
    val singleton: ^a elt ->> ^a t
    val remove: _ elt & const ->> ^a t -@>> ^a t
    val union: ^a t ->> ^a t -@>> ^a t
    val inter: ^a t ->> ^a t -@>> ^a t
    val diff: ^a t ->> ^a t -@>> ^a t
    val compare: _ t & const ->> _ t & const -@>> int
    val equal: _ t & const ->> _ t & const -@>> bool
    val subset: _ t & const ->> _ t & const -@>> bool
    val iter: <Seq@>(^a elt ~>> unit) ->> ^a t ~@>> unit
    val fold: <Seq@>(^b elt ->> ^a ~>> ^a) ->> ^b t ->> ^a ~@>> ^a
    val for_all: <Seq@>(^b elt ~>> bool) ->> ^b t ~@>> bool
    val exists: <Seq@>(^b elt ~>> bool) ->> t ~@>> bool
    type (@r,!~) predicate = { predicate : (^a:Seq@) . <Seq@r>(^a t ~@>> bool) } [@@unboxed]
    val filter_own: (@,!~) predicate ->> ^a t ~@>> ^a t
    val partition_own: (@,!~) predicate ->> ^a t ~@>> ^a t * ^a t
    val filter: (^a:Seq@).<Seq@>(^a elt ~>> bool) ->> ^a t ~@>> ^a t
    val partition: (^a:Seq@).<Seq@>(^a elt ~>> bool) ->> ^a t ~@>> ^a t * ^a t
    val cardinal: _ t & const -@>> int
    val elements: ^a t -@>> ^a elt list
    val min_elt: ^a t -@>> ^a elt
    val max_elt: ^a t -@>> ^a elt
    val choose: ^a t -@>> ^a elt
    val split: _ elt & const ->> ^a t -@>> ^a t * bool * ^a t
    val find: _ elt & const ->> ^a t -@>> ^a elt
    val of_list: ^a elt list -@>> ^a t
  end

type 'elt tree = Empty | Node of 'elt tree * 'elt * 'elt tree * int

module MakeGen
    (E : sig
       effect + !r eff
     end)
    (Ord : sig
       type ^a t : Own@r
       val compare: _ t & const ->> _ t & const -[.. as ![] E.eff]@>> int
     end) =
  struct
    (* Sets are represented by balanced binary trees (the heights of the
       children differ by at most 2 *)

    let height = function
        Empty -> 0
      | &Node(_, _, _, h) -> h

    (* Creates a new node with left son l, value v and right son r.
       We must have all elements of l < v < all elements of r.
       l and r must be balanced and | height l - height r | <= 2.
       Inline expansion of height for better speed. *)

    let create *l *v *r =
      let open Int_compare in
      let hl = match &l with Empty -> 0 | Node(_,_,_,h) -> h in
      let hr = match &r with Empty -> 0 | Node(_,_,_,h) -> h in
      Node(l, v, r, (if hl >= hr then hl + 1 else hr + 1))

    (* Same as create, but performs one step of rebalancing if necessary.
       Assumes l and r balanced and | height l - height r | <= 3.
       Inline expansion of create for better speed in the most frequent case
       where no rebalancing is required. *)

    let bal *l *v *r =
      let open Int_compare in
      let hl = match &l with Empty -> 0 | Node(_,_,_,h) -> h in
      let hr = match &r with Empty -> 0 | Node(_,_,_,h) -> h in
      if hl > hr + 2 then begin
        match l with
          Empty -> invalid_arg "Set.bal"
        | *Node(ll, lv, lr, _) ->
            if height &ll >= height &lr then
              create ll lv (create lr v r)
            else begin
              match lr with
                Empty -> invalid_arg "Set.bal"
              | Node(lrl, lrv, lrr, _)->
                  create (create ll lv lrl) lrv (create lrr v r)
            end
      end else if hr > hl + 2 then begin
        match r with
          Empty -> invalid_arg "Set.bal"
        | *Node(rl, rv, rr, _) ->
            if height rr >= height rl then
              create (create l v rl) rv rr
            else begin
              match rl with
                Empty -> invalid_arg "Set.bal"
              | Node(rll, rlv, rlr, _) ->
                  create (create l v rll) rlv (create rlr rv rr)
            end
      end else
        Node(l, v, r, (if hl >= hr then hl + 1 else hr + 1))

    (* Insertion of one element *)

    let rec add *x = function
        Empty -> Node(Empty, x, Empty, 1)
      | *Node(l, v, r, _) as t ->
          let open Int_compare in
          let c = Ord.compare const&x const&v in
          if c = 0 then t else
          if c < 0 then bal (add x l) v r else bal l v (add x r)

    let singleton *x = Node(Empty, x, Empty, 1)

    (* Beware: those two functions assume that the added v is *strictly*
       smaller (or bigger) than all the present elements in the tree; it
       does not test for equality with the current min (or max) element.
       Indeed, they are only used during the "join" operation which
       respects this precondition.
    *)

    let rec add_min_element *v = function
      | Empty -> singleton v
      | *Node (l, x, r, h) ->
        bal (add_min_element v l) x r

    let rec add_max_element *v = function
      | Empty -> singleton v
      | *Node (l, x, r, h) ->
        bal l x (add_max_element v r)

    (* Same as create and bal, but no assumptions are made on the
       relative heights of l and r. *)

    let rec join *l *v *r =
      match (l, r) with
        (Empty, _) -> add_min_element v r
      | (_, Empty) -> add_max_element v l
      | (Node(ll, lv, lr, lh), Node(rl, rv, rr, rh)) ->
          let open Int_compare in
          if lh > rh + 2 then bal ll lv (join lr v r) else
          if rh > lh + 2 then bal (join l v rl) rv rr else
          create l v r

    (* Smallest and greatest element of a set *)

    let rec min_elt = function
        Empty -> raise Not_found
      | *Node(Empty, v, r, _) -> v
      | *Node(l, v, r, _) -> min_elt l

    let rec max_elt = function
        Empty -> raise Not_found
      | *Node(l, v, Empty, _) -> v
      | *Node(l, v, r, _) -> max_elt r

    (* Remove the smallest element of the given set *)

    let rec remove_min_elt = function
        Empty -> invalid_arg "Set.remove_min_elt"
      | *Node(Empty, v, r, _) -> v, r
      | *Node(l, v, r, _) ->
         let *l, *min = remove_min_elt l in
         bal l v r, min

    (* Merge two trees l and r into one.
       All elements of l must precede the elements of r.
       Assume | height l - height r | <= 2. *)

    let merge *t1 *t2 =
      match (t1, t2) with
        (Empty, t) -> t
      | (t, Empty) -> t
      | (_, _) ->
         let *l, *min = remove_min_elt t2 in
         bal t1 min l

    (* Merge two trees l and r into one.
       All elements of l must precede the elements of r.
       No assumption on the heights of l and r. *)

    let concat *t1 *t2 =
      match (t1, t2) with
        (Empty, t) -> t
      | (t, Empty) -> t
      | (_, _) ->
         let *l, *min = remove_min_elt t2 in
         join t1 min l

    (* Splitting.  split x s returns a triple (l, present, r) where
        - l is the set of elements of s that are < x
        - r is the set of elements of s that are > x
        - present is false if s contains no element equal to x,
          or true if s contains an element equal to x. *)

    let rec split &x = function
        Empty ->
          (Empty, false, Empty)
      | *Node(l, v, r, _) ->
          let open Int_compare in
          let c = Ord.compare x &v in
          if c = 0 then (l, true, r)
          else if c < 0 then
            let *(ll, pres, rl) = split x l in (ll, pres, join rl v r)
          else
            let *(lr, pres, rr) = split x r in (join l v lr, pres, rr)

    (* Implementation of the set operations *)

    let empty = &Empty

    let make_empty () = Empty

    let is_empty = function Empty -> true | _ -> false

    let rec mem x = function
        Empty -> false
      | *Node(l, v, r, _) ->
          let open Int_compare in
          let c = Ord.compare x const&v in
          c = 0 || mem x (if c < 0 then l else r)

    let rec remove x = function
        Empty -> Empty
      | *Node(l, v, r, _) ->
          let open Int_compare in
          let c = Ord.compare x const&v in
          if c = 0 then merge l r else
          if c < 0 then bal (remove x l) v r else bal l v (remove x r)

    let rec union *s1 *s2 =
      match (s1, s2) with
        (Empty, t2) -> t2
      | (t1, Empty) -> t1
      | (Node(l1, v1, r1, h1), Node(l2, v2, r2, h2)) ->
          let open Int_compare in
          if h1 >= h2 then
            if h2 = 1 then add v2 s1 else begin
              let (l2, _, r2) = split const&v1 s2 in
              join (union l1 l2) v1 (union r1 r2)
            end
          else
            if h1 = 1 then add v1 s2 else begin
              let (l1, _, r1) = split const&v2 s1 in
              join (union l1 l2) v2 (union r1 r2)
            end

    let rec inter *s1 *s2 =
      match (s1, s2) with
        (Empty, t2) -> Empty
      | (t1, Empty) -> Empty
      | (Node(l1, v1, r1, _), t2) ->
          match split &v1 t2 with
            (l2, false, r2) ->
              concat (inter l1 l2) (inter r1 r2)
          | (l2, true, r2) ->
              join (inter l1 l2) v1 (inter r1 r2)

    let rec diff *s1 *s2 =
      match (s1, s2) with
        (Empty, t2) -> Empty
      | (t1, Empty) -> t1
      | (Node(l1, v1, r1, _), t2) ->
          match split const&v1 t2 with
            (l2, false, r2) ->
              join (diff l1 l2) v1 (diff r1 r2)
          | (l2, true, r2) ->
              concat (diff l1 l2) (diff r1 r2)

    type ^a enumeration = End | More of ^a Ord.t * ^a Ord.t tree * ^a enumeration

    let rec cons_enum s e =
      match s with
        Empty -> e
      | Node(l, v, r, _) -> cons_enum l (More(v, r, e))

    let rec compare_aux e1 e2 =
        match (e1, e2) with
        (End, End) -> 0
      | (End, _)  -> -1
      | (_, End) -> 1
      | (More(v1, r1, e1), More(v2, r2, e2)) ->
          let open Int_compare in
          let c = Ord.compare v1 v2 in
          if c <> 0
          then c
          else compare_aux (cons_enum r1 e1) (cons_enum r2 e2)

    let compare s1 s2 =
      compare_aux (cons_enum s1 End) (cons_enum s2 End)

    let equal s1 s2 =
      Int_compare.(=) (compare s1 s2) 0

    let rec subset s1 s2 =
      match (s1, s2) with
        Empty, _ ->
          true
      | _, Empty ->
          false
      | Node (l1, v1, r1, _), (Node (l2, v2, r2, _) as t2) ->
          let open Int_compare in
          let c = Ord.compare v1 v2 in
          if c = 0 then
            subset l1 l2 && subset r1 r2
          else if c < 0 then
            subset (Node (l1, v1, Empty, 0)) l2 && subset r1 t2
          else
            subset (Node (Empty, v1, r1, 0)) r2 && subset l1 t2

    let rec iter &f = function
        Empty -> ()
      | *Node(l, v, r, _) -> iter f l; f v; iter f r

    let rec fold &f *s *accu =
      match s with
        Empty -> accu
      | *Node(l, v, r, _) -> fold f r (f v (fold f l accu))

    let rec for_all &p = function
        Empty -> true
      | *Node(l, v, r, _) -> p v && for_all p l && for_all p r

    let rec exists &p = function
        Empty -> false
      | *Node(l, v, r, _) -> p v || exists p l || exists p r

    let rec filter_own &p = function
        Empty -> Empty
      | *Node(l, v, r, _) ->
          (* call [p] in the expected left-to-right order *)
          let l' = filter p l in
          let pv = p.predicate &v in
          let r' = filter p r in
          if pv then join l' v r' else concat l' r'

    let filter predicate = filter_own { predicate }

    let rec partition &p = function
        Empty -> (Empty, Empty)
      | *Node(l, v, r, _) ->
          (* call [p] in the expected left-to-right order *)
          let (lt, lf) = partition p l in
          let pv = p.predicate &v in
          let (rt, rf) = partition p r in
          if pv
          then (join lt v rt, concat lf rf)
          else (concat lt rt, join lf v rf)

    let partition predicate = partition_own { predicate }

    let rec cardinal = function
        Empty -> 0
      | Node(l, v, r, _) -> cardinal l + 1 + cardinal r

    let rec elements_aux *accu = function
        Empty -> accu
      | *Node(l, v, r, _) -> elements_aux (v :: elements_aux accu r) l

    let elements *s =
      elements_aux [] s

    let choose = min_elt

    let rec find x = function
        Empty -> raise Not_found
      | Node(l, v, r, _) ->
          let open Int_compare in
          let c = Ord.compare x const&v in
          if c = 0 then v
          else find x (if c < 0 then l else r)

    let of_sorted_list *l =
      let rec sub n *l =
        match n, l with
        | 0, l -> Empty, l
        | 1, x0 :: l -> Node (Empty, x0, Empty, 1), l
        | 2, x0 :: x1 :: l -> Node (Node(Empty, x0, Empty, 1), x1, Empty, 2), l
        | 3, x0 :: x1 :: x2 :: l ->
            Node (Node(Empty, x0, Empty, 1), x1, Node(Empty, x2, Empty, 1), 2),l
        | n, l ->
          let nl = n / 2 in
          let left, l = sub nl l in
          match l with
          | [] -> assert false
          | mid :: l ->
            let right, l = sub (n - nl - 1) l in
            create left mid right, l
      in
      fst (sub (List.length &l) l)

    let of_list *l =
      match l with
      | [] -> empty
      | [x0] -> singleton x0
      | [x0; x1] -> add x1 (singleton x0)
      | [x0; x1; x2] -> add x2 (add x1 (singleton x0))
      | [x0; x1; x2; x3] -> add x3 (add x2 (add x1 (singleton x0)))
      | [x0; x1; x2; x3; x4] -> add x4 (add x3 (add x2 (add x1 (singleton x0))))
      | _ -> of_sorted_list (List.sort_uniq_own { List.comparison = Ord.compare } l)
  end

module Pure = struct
  effect !r eff = !r
end

module Impure = struct
  effect !r eff = ![io | !r]
end

module Make (Ord : OrderedType) =
  struct
    include MakeGen(Impure)(struct type ^a t = Ord.t let compare = Ord.compare end)
    type elt = Ord.t
    type t = elt tree
  end

module MakePoly (Ord : OrderedTypePure) : SPoly with type ^a elt = ^a Ord.t =
  struct
    module Impl = MakeGen(Pure)(Ord)
    type ^a elt = ^a Ord.t
    type ^a t = ^a elt tree
  end
