(* GMM 2018. Original copyright retained: *)
(***********************************************************************)
(*                                                                     *)
(*                                OCaml                                *)
(*                                                                     *)
(*            Xavier Leroy, projet Cristal, INRIA Rocquencourt         *)
(*                                                                     *)
(*  Copyright 1996 Institut National de Recherche en Informatique et   *)
(*  en Automatique.  All rights reserved.  This file is distributed    *)
(*  under the terms of the GNU Library General Public License, with    *)
(*  the special exception on linking described in file ../LICENSE.     *)
(*                                                                     *)
(***********************************************************************)

type ^a t = { unique mutable c : ^a list; mutable len : int; }

exception Empty

let create () = { c = []; len = 0; }

let swap &s &s' = s.c <-> s'.c; s.len <-> s'.len

let take &s =
  let s' = create () in
  swap s s' ; s'

let clear &s = s.c <- []; s.len <- 0

let copy s = { c = s.c; len = s.len; }

let push *x &s =
  let { *c ; len } = take s in
  s.c <- x :: c; s.len <- len + 1

let pop &s =
  let { *c ; len } = take s in
  match c with
  | *hd::*tl -> s.c <- tl; s.len <- len - 1; hd
  | []     -> raise Empty

let pop_opt &s =
  let { *c ; len } = take s in
  match c with
  | *hd::*tl -> s.c <- tl; s.len <- len - 1; Some hd
  | []     -> None

let top &s =
  match s.c with
  | &hd::_ -> hd
  | []    -> raise Empty

let top_opt &s =
  match s.c with
  | &hd::_ -> Some hd
  | []    -> None

let is_empty s = (s.c = [])

let length s = s.len

let iter &f &s = List.iter f s.c

let fold &f *acc &s = List.fold_left f acc s.c

(** {1 Iterators} *)
(* TODO *)

let to_seq s = List.to_seq s.c

let add_seq q i = Seq.iter (fun x -> push x q) i

let of_seq g =
  let s = create() in
  add_seq s g;
  s
