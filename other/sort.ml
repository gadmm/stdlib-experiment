(*#use "topfind";;
#require "benchmark";;*)

open List

let rec chop k l =
  if k = 0 then l else begin
    match l with
    | _::t -> chop (k-1) t
    | _ -> assert false
  end


(* algorithm from std lib *)
let stable_sort cmp l =
  let rec rev_merge l1 l2 accu =
    match l1, l2 with
    | [], l2 -> rev_append l2 accu
    | l1, [] -> rev_append l1 accu
    | h1::t1, h2::t2 ->
        if cmp h1 h2 <= 0
        then rev_merge t1 l2 (h1::accu)
        else rev_merge l1 t2 (h2::accu)
  in
  let rec rev_merge_rev l1 l2 accu =
    match l1, l2 with
    | [], l2 -> rev_append l2 accu
    | l1, [] -> rev_append l1 accu
    | h1::t1, h2::t2 ->
        if cmp h1 h2 > 0
        then rev_merge_rev t1 l2 (h1::accu)
        else rev_merge_rev l1 t2 (h2::accu)
  in
  let rec sort n l =
    match n, l with
    | 2, x1 :: x2 :: _ ->
       if cmp x1 x2 <= 0 then [x1; x2] else [x2; x1]
    | 3, x1 :: x2 :: x3 :: _ ->
       if cmp x1 x2 <= 0 then begin
         if cmp x2 x3 <= 0 then [x1; x2; x3]
         else if cmp x1 x3 <= 0 then [x1; x3; x2]
         else [x3; x1; x2]
       end else begin
         if cmp x1 x3 <= 0 then [x2; x1; x3]
         else if cmp x2 x3 <= 0 then [x2; x3; x1]
         else [x3; x2; x1]
       end
    | n, l ->
       let n1 = n asr 1 in
       let n2 = n - n1 in
       let l2 = chop n1 l in
       let s1 = rev_sort n1 l in
       let s2 = rev_sort n2 l2 in
       rev_merge_rev s1 s2 []
  and rev_sort n l =
    match n, l with
    | 2, x1 :: x2 :: _ ->
       if cmp x1 x2 > 0 then [x1; x2] else [x2; x1]
    | 3, x1 :: x2 :: x3 :: _ ->
       if cmp x1 x2 > 0 then begin
         if cmp x2 x3 > 0 then [x1; x2; x3]
         else if cmp x1 x3 > 0 then [x1; x3; x2]
         else [x3; x1; x2]
       end else begin
         if cmp x1 x3 > 0 then [x2; x1; x3]
         else if cmp x2 x3 > 0 then [x2; x3; x1]
         else [x3; x2; x1]
       end
    | n, l ->
       let n1 = n asr 1 in
       let n2 = n - n1 in
       let l2 = chop n1 l in
       let s1 = sort n1 l in
       let s2 = sort n2 l2 in
       rev_merge s1 s2 []
  in
  let len = length l in
  if len < 2 then l else sort len l


(* version that works with linear values *)
let stable_sort_own cmp l =
  let rec rev_merge l1 l2 accu =
    match l1, l2 with
    | [], l2 -> rev_append l2 accu
    | l1, [] -> rev_append l1 accu
    | h1::t1, h2::t2 ->
        if cmp h1 h2 <= 0
        then rev_merge t1 l2 (h1::accu)
        else rev_merge l1 t2 (h2::accu)
  in
  let rec rev_merge_rev l1 l2 accu =
    match l1, l2 with
    | [], l2 -> rev_append l2 accu
    | l1, [] -> rev_append l1 accu
    | h1::t1, h2::t2 ->
        if cmp h1 h2 > 0
        then rev_merge_rev t1 l2 (h1::accu)
        else rev_merge_rev l1 t2 (h2::accu)
  in
  let rec sort n l =
    match n, l with
    | 2, x1 :: x2 :: tl ->
       (if cmp x1 x2 <= 0 then [x1; x2] else [x2; x1]), tl
    | 3, x1 :: x2 :: x3 :: tl ->
       (if cmp x1 x2 <= 0 then begin
         if cmp x2 x3 <= 0 then [x1; x2; x3]
         else if cmp x1 x3 <= 0 then [x1; x3; x2]
         else [x3; x1; x2]
       end else begin
         if cmp x1 x3 <= 0 then [x2; x1; x3]
         else if cmp x2 x3 <= 0 then [x2; x3; x1]
         else [x3; x2; x1]
       end), tl
    | n, l ->
       let n1 = n asr 1 in
       let n2 = n - n1 in
       let s1, l2 = rev_sort n1 l in
       let s2, tl = rev_sort n2 l2 in
       rev_merge_rev s1 s2 [], tl
  and rev_sort n l =
    match n, l with
    | 2, x1 :: x2 :: tl ->
       (if cmp x1 x2 > 0 then [x1; x2] else [x2; x1]), tl
    | 3, x1 :: x2 :: x3 :: tl ->
       (if cmp x1 x2 > 0 then begin
         if cmp x2 x3 > 0 then [x1; x2; x3]
         else if cmp x1 x3 > 0 then [x1; x3; x2]
         else [x3; x1; x2]
       end else begin
         if cmp x1 x3 > 0 then [x2; x1; x3]
         else if cmp x2 x3 > 0 then [x2; x3; x1]
         else [x3; x2; x1]
       end), tl
    | n, l ->
       let n1 = n asr 1 in
       let n2 = n - n1 in
       let s1, l2 = sort n1 l in
       let s2, tl = sort n2 l2 in
       rev_merge s1 s2 [], tl
  in
  let len = length l in
  if len < 2 then l else let s, _ = sort len l in s


(* optimised for small lists, but not for linear values *)
let stable_sort_mix cmp l =
  let rec rev_merge l1 l2 accu =
    match l1, l2 with
    | [], l2 -> rev_append l2 accu
    | l1, [] -> rev_append l1 accu
    | h1::t1, h2::t2 ->
        if cmp h1 h2 <= 0
        then rev_merge t1 l2 (h1::accu)
        else rev_merge l1 t2 (h2::accu)
  in
  let rec rev_merge_rev l1 l2 accu =
    match l1, l2 with
    | [], l2 -> rev_append l2 accu
    | l1, [] -> rev_append l1 accu
    | h1::t1, h2::t2 ->
        if cmp h1 h2 > 0
        then rev_merge_rev t1 l2 (h1::accu)
        else rev_merge_rev l1 t2 (h2::accu)
  in
  let sort_small n l =
    match n, l with
    | 2, x1 :: x2 :: _ ->
       if cmp x1 x2 <= 0 then [x1; x2] else [x2; x1]
    | 3, x1 :: x2 :: x3 :: _ ->
       if cmp x1 x2 <= 0 then begin
           if cmp x2 x3 <= 0 then [x1; x2; x3]
           else if cmp x1 x3 <= 0 then [x1; x3; x2]
           else [x3; x1; x2]
         end else begin
           if cmp x1 x3 <= 0 then [x2; x1; x3]
           else if cmp x2 x3 <= 0 then [x2; x3; x1]
           else [x3; x2; x1]
         end
    | _, _ -> assert false
  in
  let rev_sort_small n l =
    match n, l with
    | 2, x1 :: x2 :: _ ->
       if cmp x1 x2 > 0 then [x1; x2] else [x2; x1]
    | 3, x1 :: x2 :: x3 :: _ ->
       if cmp x1 x2 > 0 then begin
           if cmp x2 x3 > 0 then [x1; x2; x3]
           else if cmp x1 x3 > 0 then [x1; x3; x2]
           else [x3; x1; x2]
         end else begin
           if cmp x1 x3 > 0 then [x2; x1; x3]
           else if cmp x2 x3 > 0 then [x2; x3; x1]
           else [x3; x2; x1]
         end
    | _, _ -> assert false
  in
  let rec sort n l =
    let n1 = n asr 1 in
    let n2 = n - n1 in
    let s1, l2 =
      if n1 <= 3 then rev_sort_small n1 l, chop n1 l
      else rev_sort n1 l
    in
    let s2, tl =
      if n2 <= 3 then rev_sort_small n2 l2, chop n2 l2
      else rev_sort n2 l2
    in
    rev_merge_rev s1 s2 [], tl
  and rev_sort n l =
    let n1 = n asr 1 in
    let n2 = n - n1 in
    let s1, l2 =
      if n1 <= 3 then sort_small n1 l, chop n1 l
      else sort n1 l
    in
    let s2, tl =
      if n2 <= 3 then sort_small n2 l2, chop n2 l2
      else sort n2 l2
    in
    rev_merge s1 s2 [], tl
  in
  let len = length l in
  if len < 2 then l
  else if len <= 3 then sort_small len l
  else let s, _ = sort len l in s


let random_list state n =
  let rec random_list l n =
    if n <= 0 then l else
      let x = Random.State.bits state in
      random_list (x::l) (n-1)
  in
  random_list [] n

let decreasing_list max =
  let rec decreasing_list l n =
    if n <= 0 then l else
      decreasing_list ((max-n)::l) (n-1)
  in
  decreasing_list [] max

let increasing_list max =
  let rec increasing_list l n =
    if n <= 0 then l else
      increasing_list (n::l) (n-1)
  in
  increasing_list [] max

let params = [4,2000000;
              10,2000000;
              15,1000000;
              25,600000;
              50,400000;
              100,120000;
              250,40000;
              500,20000;
              1000,8000;
              1500,5000;
              2500,3000;
              10005,600;
              30005,200;
              100005,50;
              300005,20;
              1000005,3;
              3000005,1]

let test_sort sort gen_list n =
  Gc.full_major ();
  let rec repeat i =
    if i <= 0 then () else begin
        sort compare (gen_list n);
        repeat (i - 1)
      end
  in
  repeat (List.assoc n params)

let old_sort = test_sort stable_sort
let new_sort = test_sort stable_sort_own
let new_sort3 = test_sort stable_sort_mix

let test make_gen_list times (length, _) =
  let fmt s = Printf.sprintf "%s%d" s length in
  let res = Benchmark.latencyN ~style:Benchmark.Nil times
              [(fmt "old", old_sort (make_gen_list ()), length);
               (fmt "new", new_sort (make_gen_list ()), length);
               (fmt "new''", new_sort3 (make_gen_list ()), length);
               (fmt "old'", old_sort (make_gen_list ()), length);]
  in
  Benchmark.tabulate res;
  print_newline()

let test_full times =
(*  print_endline "Decreasing lists";
  List.iter (test (fun () -> decreasing_list) times) params;
  print_endline "Increasing lists";
  List.iter (test (fun () -> decreasing_list) times) params;
  print_endline "Random lists";*)
  let seed = 6190127954 (* chosen by fair dice roll *)
  in
  let random_states = ref [] in
  let make_gen_random () =
    let state = Random.State.make [|seed|] in
    random_states := state :: !random_states;
    random_list state
  in
  List.iter (test make_gen_random times) params;
  (* check that the random states are the same *)
  List.iter (fun state -> Random.State.bits state
                          |> Printf.printf "%d\n")
    !random_states

let _ = test_full 40L

