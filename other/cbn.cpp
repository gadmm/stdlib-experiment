// -*- C++ -*-
/* C++ lazy smart pointer.
   GMM 12/17

   This smart pointer computes the closure being passed only if the
   result is actually used. The closure can own resources, which are
   freed once the result is computed or, otherwise, when the pointer
   itself is destroyed.

   This illustrates the positive interpretation of call-by-need as the
   composition of an affine modality and a duplication modality.

   This is not very sound for sharing between threads. In fact, races
   would be very likely if the computation takes a long time. One
   could program an atomic version but there would still be an
   inherent non-determinism about who has to pay the price of
   computation, and there would be a big critical section.
 */

#include <string>
#include <iostream>
#include <memory>
#include <functional>

/* Affine modality for memoization.
   Non-copiable. */
template <typename X>
class delayed_ptr {
	mutable std::unique_ptr<std::function<X()>> closure_;
	mutable std::unique_ptr<X> result_ = nullptr;

public:
	delayed_ptr(std::function<X()> f)
		: closure_(std::make_unique<std::function<X()>>(move(f)))
	{}

	X* operator->() const
	{
		if (!result_) {
			result_ = std::make_unique<X>((*closure_)());
			closure_ = nullptr;
		}
		return result_.get();
	}

	/* other operations are automatically deduced:
	   destructor and move operations. */
};


/* Lazy pointer. Copiable (using reference counting). */
template <typename X>
using lazy_ptr = std::shared_ptr<delayed_ptr<X>>;

template <typename F>
auto make_lazy_ptr(F f)
{
	return std::make_shared<delayed_ptr<typename std::result_of<F&&()>::type>>(std::move(f));
}


// a generic getter based on operator-> overload resolution
template <typename X>
auto get(X x) { return *(x->operator->()); }


int main()
{
	return x = make_lazy_ptr([](){
			std::cout << "x computed" << "\n";
			return "x";
		});
  /* x used twice */
	std::cout << get(x) << "\n";
	std::cout << get(x) << "\n";
	return y = make_lazy_ptr([](){
			std::cout << "y computed" << "\n";
			return "y";
		});
  /* y not used */
	(void) y;
	return 0;
}
