(* #require "str";;*)

let file = "value"
let message = "Hello!"

type status = Bad | Good | Weird

let show_contents (c, s) =
  if s = Good then
    print_endline c

let read_status = function
  | "x" -> Some Bad
  | "v" -> Some Good
  | "?" -> Some Weird
  | _ -> None

let read_contents =
  let reg = Str.regexp "^\\([^#][^/]*\\)/" in
  fun l ->
    if Str.string_match reg l 0 then
      Some (Str.matched_group 1 l)
    else
      None

let num =
  (* Read file and display the first line *)
  let ic = open_in file in
  let status = ref Weird in
  let switch_status l =
    match read_status l with
    | Some s -> status := s ; true
    | None -> false
  in
  let contents = ref [] in
  begin
    try
      while true do
        let line = input_line ic in  (* read line from in_channel and discard \n *)
        let is_status = switch_status line in
        if not is_status then
          match read_contents line with
          | Some cont -> contents := (cont, !status) :: !contents
          | _ -> ()
      done ;
      close_in ic                  (* close the input channel *)
    with e ->                      (* some unexpected exception occurs *)
      close_in_noerr ic;           (* emergency closing *)
  end ;
  let cmp (a,_) (b,_) = compare a b in
  contents := List.sort_uniq cmp !contents;
  List.iter show_contents !contents ;
  flush stdout ;                (* write on the underlying device now *)
  List.length !contents
