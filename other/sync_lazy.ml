(** Rough design for decoupling lazy forcing from its synchronisation,
    and making the lazy pattern-matching always do what is intended.
    _Pseudo-code_. 11/2021. *)

(** A) Pre-requisites *)

(* The following barriers do not exist (yet?). They can be simulated
   with making the load/stores to the thunk status field OCaml-atomic
   (which is much stronger than we need). (The load/load barrier is
   already needed for reading a forward_tag in current OCaml
   multicore, AFAICT, otherwise we could probably do without them.) *)

(** Prevent load/load reordering by compiler; emit [DMB ISHLD] or
   stronger on AArch64. *)
external load_load_barrier : unit -> unit = "%acquire_fence?"

(** Prevent store/store reordering by compiler; emit [DMB ISHST] or
   stronger on AArch64. *)
external store_store_barrier : unit -> unit = "%release_fence?"

(* for our examples *)
module Domain = struct
  external get_id : unit -> int = "caml_ml_domain_id"
  external cpu_relax : unit -> unit = "caml_ml_domain_cpu_relax"
end


(** B) Minimal demo. The simplest proof of concept is to re-implement
    the multicore behaviour (after merging Undefined and RacyLazy)
    along the following lines. (_Pseudo-code_) *)

(* Comparison with current implementation:

   - The main difference is to store the thunk and the result in two
   separate fields, and using the thunk field instead of the tag to
   store the Forwing/Forward bits. Other differences are only
   stylistic or potential performance differences (good or bad).

   - It is the same technique as used in the full demo further below.
   If this minimal demo works and is performant, then the rest works
   too for custom synchronisation and custom exception-safety.

   - No C calls except write barrier. (Benefits to explore; maybe
   this is only stylistic.)

   - This is a version without CAS loops (no mutation of tags). One
   needs an extra test in the fast path which needs careful evaluation
   and optimisation (there are obvious variations to explore).

   - An alternative is to keep the Forward_tag and one CAS loop for a
   smaller difference with the OCaml 4 implementation.

   - Pattern-matching in pseudo-code (might need to use Obj in a real
   implementation, like the current implementation).

   - Reminder to deal with #7301 (though there might be a different
   reason this is not done in multicore currently).

   - Reminder to deal with interrupt-safety.
*)

type 'a thunk_status =
  | Forward
  | Forcing
  | Thunk of (unit -> 'a) [@unboxed]
and 'a t =
    | Immediate (* short-circuited by GC *)
    | Block_with_regular_tag of Obj.t (* short-circuited by GC *)
    (* ... *)
  | Lazy_tag of { mutable thunk : 'a thunk_status ;
                  mutable result : 'a }

let force_gen ~only_val block =
  (* BEGIN ATOMIC *)
  let block = Sys.opaque_identity block (* #7301 *) in
  match block with
  | Lazy_tag ({ thunk = Forward ; _ } as l) -> begin
      load_load_barrier () ;
      l.result
      (* END ATOMIC *)
    end
  | Lazy_tag { thunk = Forcing ; _ } ->
    (* END ATOMIC *)
    raise Lazy.Undefined
  | Lazy_tag ({ thunk = Thunk f ; _ } as l) -> begin
      let atomic_thunk = (Obj.magic block : 'a thunk_status Atomic.t) in
      if not (Atomic.compare_and_set atomic_thunk (Thunk f) Forcing) then
        raise Lazy.Undefined
      else begin
        (* END ATOMIC *)
        (* Races with async exn only if only_val=false. *)
        (* We now hold a lock on the lazy block, so we know that it
           still has tag Lazy_tag after f returns. *)
        match f () with
        | v -> begin
            l.result <- v ;
            store_store_barrier () ;
            l.thunk <- Forward ;
            v
          end
        | exception e when not only_val -> begin
            l.thunk <- Thunk (fun () -> raise e) ;
            raise e
          end
      end
    end
  | v -> Obj.magic v

let force l = force_gen ~only_val:false l
let force_val l = force_gen ~only_val:true l

let from_fun f = Lazy_tag { thunk = Thunk f ; result = Obj.magic () }


(** C) Full demo: scale to a wide choice of custom synchronization
    schemes and exception-safety options. *)

(* TODO list

   Done:

   - Examples: Default, Spin, Mutex, Lock-free.

   - Minimal demo and comparison with current implementation.

   - Races with GC short-circuiting & Flambda reordering.

   - Races with other threads.

   - Exception-handling (Prefer force_val behaviour for new abstractions
   when appropriate; old behaviour for the default one, and deprecate
   force_val.)

   - Async-exn safety & various ways to handle failure.

   - Comment about parametrization as effects.

   - Investigate feasability of runtime changes (implementation of
     short-circuiting, etc.).
     see lambda/{matching,translcore}.ml
     see runtime/ (grep Forward_tag)

   TODO :

   - Check if fixes for #713 are still needed.

   - Deal with remaining races with interrupts.

   - Check if inlining works.

   - Try simpler version without ~only_val ; stricter deprecation of
   the force/force_val split (even if the default behaviour gets even
   less well defined under races).
*)

(** C.1) Low level *)
module CamlinternalLazy = struct
  type 'a thunk_status =
    | Forward
    | Thunk of (only_val:bool -> 'a t -> 'a) [@unboxed]
  and 'a t =
    | Immediate (* short-circuited by GC *)
    | Block_with_regular_tag of Obj.t (* short-circuited by GC *)
    (* ... *)
    | Lazy_tag of { mutable thunk : 'a thunk_status ;
                    mutable result : 'a }

  (** Define a new lazy synchronisation. The thunk takes the lazy
      block as argument. *)
  let make (type a) (f : (only_val:bool -> a t -> a)) =
    Lazy_tag { thunk = Thunk f ; result = (Obj.magic () : a) }

  (* BEGIN ATOMIC...END ATOMIC: atomic for the GC (i.e. polling
     critical section), to avoid races with the GC short-circuiting
     the Forward lazy value. Short-circuiting can happen:

     1) In parallel by the GC, rewriting block fields under our feets.

     2) By the minor GC at polling points, rewriting registers under
     our feets.

     Thus the rule is simple: knowledge that the tag is Lazy_tag only
     holds until next polling location and next re-reading of the
     block address from its parent.

     In addition, according to #7301/#731 we need to prevent flambda
     from forwarding previously-known values of the tag which it
     assumes to be immutable, since this is invalidated at polling
     points. An opaque_identity is enough. *)

  (** Lock-less forcing with duplicate evaluation (_pseudo-code_); the
      paper "Haskell on a Shared-Memory Multiprocessor" (2005) gives
      more details on how races are avoided; importantly the value and
      the thunk occupy two distinct fields (if I translate correctly, we
      need a load/load barrier simulating an acquire load of the thunk
      field). *)
  let force_gen ~only_val block =
    (* BEGIN ATOMIC *)
    let block = Sys.opaque_identity block (* #7301 *) in
    match block with
    | Lazy_tag ({ thunk = Forward ; _ } as l) -> begin
        load_load_barrier () ;
        l.result
        (* END ATOMIC *)
      end
    | Lazy_tag { thunk = Thunk f ; _ } as l ->
      (* END ATOMIC *)
      f ~only_val l
    | v -> Obj.magic v

  let get_val block =
    (* BEGIN ATOMIC *)
    let block = Sys.opaque_identity block (* #7301 *) in
    match block with
    | Lazy_tag { result ; _ } -> result (* END ATOMIC *)
    | v -> Obj.magic v

  (** Updating is handled by the thunk; see Haskell paper for how races
      are avoided (if I translate correctly, we need a store/store
      barrier simulating a release store of the thunk field).
      Lock-less. *)
  let set_val block v =
    (* BEGIN ATOMIC *)
    let block = Sys.opaque_identity block (* #7301 *) in
    match block with
    | Lazy_tag l -> begin
        l.result <- v ;
        store_store_barrier () ;
        l.thunk <- Forward ;
        (* END ATOMIC *)
        v
      end
    | v -> Obj.magic v

  (* [%atomic_cas] with an offset; does not exist yet *)
  external array_compare_and_set : 'a t -> int -> 'a -> 'a -> bool = "%atomic_array_cas"

  let set_val_atomic block v =
    (* BEGIN ATOMIC *)
    let block = Sys.opaque_identity block (* #7301 *) in
    match block with
    | Lazy_tag l -> begin
        if array_compare_and_set block 1 (Obj.magic ()) v
        (* END ATOMIC *)
        then v
        else get_val block
      end
    | v -> Obj.magic v

  let set_thunk block f =
    (* BEGIN ATOMIC *)
    let block = Sys.opaque_identity block (* #7301 *) in
    match block with
    | Lazy_tag l -> l.thunk <- Thunk f (* END ATOMIC *)
    | _ -> ()

  let compare_and_set_thunk block old_thunk new_thunk =
    (* BEGIN ATOMIC *)
    let block = Sys.opaque_identity block (* #7301 *) in
    match block with
    | Lazy_tag _ ->
      let atomic_thunk = (Obj.magic block : 'a thunk_status Atomic.t) in
      Atomic.compare_and_set atomic_thunk old_thunk new_thunk
      (* END ATOMIC *)
    | _ -> false

  let is_val block =
    let block = Sys.opaque_identity block (* #731 ? *) in
    (* BEGIN ATOMIC *)
    match block with
    | Lazy_tag { thunk = Forward ; _ } -> begin
        (* END ATOMIC *)
        load_load_barrier () ; (* nicety so that we can call [get_val]
                                  right after *)
        true
      end
    | Lazy_tag _ -> false
    | _ -> true

end (* module CamlinternalLazy *)


(** C.2) High level *)
module Lazy = struct
  type 'a t = 'a CamlinternalLazy.t

  let force l = CamlinternalLazy.force_gen ~only_val:false l

  (* To deprecate. The behaviour on exception arising should be
     defined by the lazy implementation. *)
  let force_val l = CamlinternalLazy.force_gen ~only_val:true l

  exception Undefined

  let fail ~only_val:_ _ = raise Undefined

  (* Unsynchronised forcing primitive.
     TODO: check whether we can inline f this way. *)
  let[@inline] do_force ~only_val block f =
    match (f [@inlined]) () with
    | v -> CamlinternalLazy.set_val block v
    | exception e when not only_val -> begin
        let new_thunk =
          if only_val then fail
          else fun ~only_val:_ -> raise e
        in
        CamlinternalLazy.set_thunk block new_thunk ;
        raise e
      end

  exception RacyLazy

  (** 1. Current multicore behaviour, but with freeing the thunk and
      with only one CAS. *)
  let[@inline] from_fun f =
    (* optional: allocate one more field in the lazy value for this
       atomic (but beware races with short-circuiting!). *)
    let id = Atomic.make (-1) in
    let check_race ~only_val:_ _ =
      if Atomic.get id = Domain.get_id ()
      then raise Undefined else raise RacyLazy
    in
    let force_f ~only_val block =
      if Atomic.compare_and_set id (-1) (Domain.get_id ()) then begin
        (* Race with async exns, leaving the thunk checking for races *)
        CamlinternalLazy.set_thunk block check_race ; (* free the thunk *)
        do_force ~only_val block f
      end
      else check_race ~only_val ()
    in
    CamlinternalLazy.make force_f

  (** 2. Simpler version of a default implementation; like the
      previous one but it does not need to allocate an atomic. *)
  let[@inline] from_fun_simpler f =
    let rec force_f ~only_val block =
      if CamlinternalLazy.compare_and_set_thunk
           block (Thunk force_f) (Thunk fail)
      then
        (* the thunk has been freed *)
        (* Races with async exn only if only_val = false. *)
        do_force ~only_val block f
      else fail ()
    in
    CamlinternalLazy.make force_f

  module type THREADS = sig
    val get_id : unit -> int
    val yield : unit -> unit
  end
  (* An alternative is parametrization as effects which can be
     expressed by parametrization as arguments. *)

  let poison_on_failure block f =
    try f () with e -> begin
        CamlinternalLazy.set_thunk block fail ;
        raise e
      end

  (** 3. Spin-waiting implementation for a custom threading library.
      Guarantees that the thunk is called at most once. If an
      exception arises during evaluation, then subsequent forcing of
      the lazy raises Undefined. *)
  module Spin (MyThreads : THREADS) = struct
    let[@inline] from_fun my_get_my_yield f =
      let id = Atomic.make (-1) in
      let rec spin ~only_val:_ block =
        if Atomic.get id <> MyThreads.get_id () then begin
          MyThreads.yield () ;
          force_val block
        end
        else raise Undefined
      in
      let force_f ~only_val block =
        if Atomic.compare_and_set id (-1) (MyThreads.get_id ()) then begin
          (* No races with async exn *)
          poison_on_failure block (fun () ->
            (* free the atomic and the thunk *)
            CamlinternalLazy.set_thunk block spin ;
            let v = (f [@inlined]) () in
            CamlinternalLazy.set_val block v )
        end
        else spin ~only_val block
      in
      CamlinternalLazy.make force_f
  end

  (* inter-domain waiting using cpu_relax is a particular case *)
  module SpinDomain =
    Spin (struct include Domain let yield = cpu_relax end : THREADS)

  module type MUTEX = sig
    type t
    val create : unit -> t
    val with_lock : t -> (unit -> 'a) -> 'a
  end

  (** 4. Synchronisation with a Mutex for a custom threading library.
      Guarantees that the thunk is called at most once. If an
      exception arises during evaluation, then subsequent forcing of
      the lazy raise Undefined. *)
  module Lock (MyMutex : MUTEX) = struct
    let[@inline] from_fun f =
      let mut = MyMutex.create () in
      let f_ref = ref CamlinternalLazy.get_val in
      let force_f ~only_val:_ block =
        MyMutex.with_lock mut (fun () -> (!f_ref) block)
      in
      f_ref := (fun block ->
        (* No races with async exn *)
        poison_on_failure block (fun () ->
          (* free the thunk *)
          f_ref := CamlinternalLazy.get_val ;
          let v = (f [@inlined]) () in
          CamlinternalLazy.set_val block v
        )
      ) ;
      CamlinternalLazy.make force_f
  end

  (** 5. Stock behaviour without synchronisation or CASes. (Variant
      that raises Undefined if a previous evaluation raised an
      exception.)

      Use cases: when the lazy is not shared between threads using
      programmer discipline or fancy type systems. However, it is
      possible that the gain compared to Lazy.from_fun_simpler is
      negligible in practice due to the low cost of uncontended CASes.

      (Memory-safe, at the cost of work duplication in case it is
      erroneously shared across threads.) *)
  let[@inline] from_fun_nosync_err f =
    let force_f ~only_val:_ lazy_block =
      (* free the thunk *)
      (* No race with async exns *)
      CamlinternalLazy.set_thunk lazy_block fail ;
      let v = (f [@inlined]) () in
      CamlinternalLazy.set_val lazy_block v
    in
    CamlinternalLazy.make force_f

  (** 6. Lock-free lazy. If two threads race to force the lazy, then
      the thunk can be evaluated twice. A single CAS is used to agree
      on the final result. If evaluating the thunk raises an
      exception, then forcing the lazy again will evaluate the thunk
      again.

      Use-cases: when [f] is purely functional and it is more
      advantageous to pay for the duplicated work on unlikely races
      than paying for synchronisation at every forcing. The paper
      "Haskell on a Shared-Memory Multiprocessor" suggests that this
      can be an advantageous strategy; however it is not clear a
      priori that the result translates to OCaml.

      For OCaml itself this could be a good strategy for someone
      wanting to implement some of Okasaki's “Purely functional data
      structures” (1996) which achieve an amortized complexity through
      lazy evaluation, which are a good candidate to being shared
      between threads. but for which the cost of synchronizing the
      evaluation of many small thunks can be higher than duplicating
      work.

      This also demontrates a lazy that can be interrupted without
      remote consequences (we can also obtain this with a spin lock;
      the cost is to delay the freeing of the thunk). *)
  let[@inline] from_fun_nosync_dup f =
    CamlinternalLazy.make (fun ~only_val:_ block ->
      let v = (f [@inlined]) () in
      (* free the thunk *)
      CamlinternalLazy.set_val_atomic block v
    )

  (* Similar to trunk *)
  let from_val (type a) (v : a) : a t =
    let t = Obj.tag (Obj.repr v) in
    if t = Obj.lazy_tag || t = Obj.double_tag then
      Lazy_tag { thunk = Forward ; result = v }
    else
      (Obj.magic v : a t)

  (* Like trunk. Note: the synchronisation mecanism for the new lazy
     can be chosen differently from the old one. *)
  let map f x = from_fun (fun () -> (f (force x)))

  let is_val = CamlinternalLazy.is_val
  let get_val = CamlinternalLazy.get_val

end (* module Lazy *)
