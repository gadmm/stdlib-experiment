(** MLworkshop challenge: design a type system for solving the slicing
    problem for linear functional data structures. *)

type res : lin

val compare : &res -> &res -> int

type +^a t = Node of t * ^a * t | Leaf
(** invariant: [Node (l, x, r)] is such that [x] is greater than any
    element in [l] according to [compare] and strictly smaller than any
    element in [r] *)

val partition : pivot:&^a -> ^a t -> ^a t * ^a t

let rec partition ~pivot = function
  | Leaf -> Leaf, Leaf
  | Node (l, y, r) when compare x &y <= 0 ->
    let l1, l2 = partition ~pivot l in
    (l1, Node (l2, y, r))
  | Node (l, y, r) (* compare x &y > 0 *) ->
    let r1, r2 = partition ~pivot r in
    (Node (l, y, r1), r2)

val big_tree : res t

(** apply a function in two parallel threads *)
let do_parallel : (^a -> unit) -> ^a * ^a -> unit

(** a reentrant function that requires exclusive access to its
    argument *)
let do_iter : &!^a t -> unit

val pivot : res

(** split the borrowed tree and call do_iter on the two slices in
    parallel *)
let () =
  partition ~&pivot &!big_tree |>
  do_parallel do_iter

(** Explanation: *)

let f = do_parallel do_iter
val f : &!^a t * &!^a t -> unit
(** [f] applied do_iter in parallel, its type asserts that the two
    arguments are disjoint *)

let g = (partition : _ -> &!^a t -> _)
val g : &^a -> &!^a t -> &!^a t * &!^a t
(** It is clear how partition can operate on a tree of borrowed
    resources, but we only have a (borrowable) tree of resources. *)

let h = &!big_tree
val h : &!(res t)
(** It all works due to an isomorphism of structure: a borrowed tree
    of resources is isomorphic to a tree of borrowed resources, since
    the skeleton of the tree itself need not be considered a resource.

      type &!(^a t) = Node of &!(^a t) * &!^a * &!(^a t) | Leaf

    in other words, correctly: *)
val h : (&!res) t

(** The type specialisation of [partition] therefore operates on a
    uniquely borrowed tree, and returns two uniquely borrowed trees of
    resources. These borrows do not refer to actual trees of
    resources, but slices of the original tree which (as a linear
    value) remains unmodified. *)

(** In terms of allocation methods, [big_tree] could very well be
    allocated in a secondary heap à la ancient (so its linearity is
    important for safe deallocation). In this case, [partition]
    allocates new cells with the GC, disregarding how its input was
    allocated. The result is two trees that mix allocation methods but
    share as much as possible with the original tree, exacly as if one
    had called [partition] on a big tree allocated with ancient.
    Moreover, this sharing is consistent with linearity, as the
    original aliases are burrowed for the duration of the original
    borrow.

    Thus, this answers the slicing problem (how to get disjoint views)
    for functional data structures allocated linearly.

    Conversely, from a language that has linear allocation, the view
    that the skeleton of the tree need not be a resource can only be
    realised if there is a GC that can allocate cells for this
    skeleton. Rust, say, loses much in expressiveness by having its
    Copy trait be restricted to values on the stack copied with
    [memcpy], and thus force ownership disciplines for heap-allocated
    values.
*)
