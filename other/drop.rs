#![allow(unused)]
#![feature(nll)]
// This example shows how Rust enforces simple protocols with RAII
// values.
//
// This is an imaginary protocol where a client has to respect the
// following order:
//
// 1) bind
// 2) connect
// 3) send/receive
// 4) close connection
// 5) unbind
//
// This principle is used for Berkeley Sockets in e.g.
// std::net::TcpListener and std::net::TcpStream.

struct Client {
    name: &'static str,
}

impl Client {
    fn bind(name: &'static str) -> Client {
        println!("< Binding {}", name);
        Client { name: name }
    }

    fn connect<'a>(&'a self, name: &'static str) -> Connection<'a> {
        println!("< Connecting {} for client {}", name, self.name);
        Connection { client: &self, name: name }
    }
}

impl Drop for Client {
    fn drop(&mut self) {
        println!("> Unbinding {}", self.name);
    }
}

struct Connection<'a> {
    client: &'a Client,
    name: &'static str,
}

impl<'a> Connection<'a> {
    fn send(&self) {
        println!("{} sending", self.name);
    }

    fn recv(&self) {
        println!("{} receive", self.name);
    }
}

impl<'a> Drop for Connection<'a> {
    fn drop(&mut self) {
        println!("> Disconnect {} for client {}", self.name, self.client.name);
    }
}

fn main() {
    let client = Client::bind("A");
    let connection = client.connect("1");
    //drop(connection);
    // ^ Try uncommenting this line
    connection.send();
    //drop(client);
    // ^ Try uncommenting this line
}
