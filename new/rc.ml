(* not (yet) provided by the Atomic module so we do it by hand *)
let fetch_add n a =
  let open Atomic in
  let rec cas () =
    let value = get a in
    if compare_and_set a value (value + n) then
      value
    else
      cas ()
  in
  cas ()

let fetch_incr = fetch_add 1
let fetch_decr = fetch_add (-1)

(* a weak pointer is a GCed value containing the count of strong
   pointers and a borrow of the resource *)
type ^a weak = int Atomic.t * ^a &@global const

let drop (count, x) =
  let n = fetch_decr count;
  unsafe
    (* drop x if it is the last one *)
    if n = 1 then ignore (LObj.take x)
  end

type ^a t = ^a weak with destructor drop

let make *x = new t (Atomic.make 1, unsafe begin LObj.leak x end)

let copy ((count, _) as p) = new t (fetch_incr count; p)

let make_weak p = p

let lock ((count, _) as p) =
  let rec incr_if_nonzero () =
    let open Atomic in
    let n = get count in
    if n = 0 then
      raise Exit
    else if not (compare_and_set count n (n + 1)) then
      incr_if_nonzero ()
  in
  try
    Some (new t (incr_if_nonzero (); p))
  with
  | Exit -> None

let get (_, x) = x

let unique *p =
  let (count, x) = &p in
  (* FIXME: we need here an RAII guard for turning off async
     exceptions. A priori rule of thumb: we only need to be careful
     about async exceptions in this way when doing resource management
     with unsafe; otherwise the critical section provided by [new] is
     enough. *)
  if Atomic.compare_and_set count 1 0 then
    unsafe begin
      Ok (LObj.take x)
    end
  else
    Error p
