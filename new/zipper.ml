type ^a tsil = Nil | Cons of ^a tsil * ^a
(* A list that deletes starting from the end. *)

type ^a zip = Zip of ^a tsil * ^a list
(* It is now important to use tsil : we do not want the traversal of a
   zipper to change the order of evaluation. *)

let from_list *l = Zip (Nil, l)

let add *x *(Zip (l, r)) = Zip (l, x::r)

let right *(Zip (l, r)) = match r with
  | x :: r' -> Zip (Cons (l, x), r')
  | [] -> failwith "right"

let has_right (Zip (_, r)) = match r with [] -> false | _ -> true

let left *(Zip (l, r)) = match l with
  | Cons (l', x) -> Zip (l', x::r)
  | Nil -> failwith "left"

let has_left (Zip (l, _)) = match l with Nil -> false | _ -> true

let current *(Zip (_, r)) = match r with
  | x :: _ -> x
  | [] -> failwith "current"

let previous *(Zip (l, _)) = match l with
  | Cons (_, x) -> x
  | Nil -> failwith "previous"
