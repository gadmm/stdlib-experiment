Lessons from writing Rc

Implementing atomic Rc pointers correctly is notoriously hard and so
this first implementation is most likely wrong :)


* Only one Rc count is needed

There are two reference counts in this implementation: one explicit
and one used for counting the GC root. An actual implementation would
probably rely on re-using the RC count machanism for the GC root (at
the cost of some assumptions about the implementation and some type
abstraction to prevent the count from being increased externally).

RC pointer implementations in C++ and Rust also maintain two reference
counts, one for weak and one for strong pointers. We found that we
only need to count strong pointers; the weak references are collected
by the tracing GC.


* Unsafe

This is an example of a module using unsafe functions form the LObj
module. In LObj we find functions to wildly take ownership of or leak
resources, and these functions perform an effect [unsafe].

Then the [unsafe] block is used to hide this effect. A module using an
[unsafe] block should be verified externally, à la Jung et al.


* Covariance without containment

A type declaration:

  type +^a t

describes a type containing some resource ^a. This declaration
entails:

  <^a t> = <^a>
  ^a t & = ^a & t
  ^a t const = ^a const t

Our types of Rc pointers are covariant, but do not satisfy such
properties: indeed Rc.t is always Own and Rc.weak is always Copy. We
use a new declaration for covariance without containment:

  type ?+^a t : Own
  type ?+^a weak : Copy


* Asynchronous exceptions and new

[new t] is a keyword for creating a value form a type with custom
destructor [t] from a value of its underlying type. Something terrible
happens if an asynchronous exception is raised between the moment when
the reference count is incremented and the moment the RAII object is
created using [new].

The prevent asynchronous exceptions from being triggered while
resources are being acquired, we set [new] to define a critical
section for exceptions. For instance, the reference count is increased
during copy with the following:

  new t (fetch_incr count; p)

No asynchronous exception can be raised between the moment the
reference count is incremented and the resource responsible for
decrementing the count is ready.

A critical section is also set in every destructor, given that
destructors should not raise.


We noticed the following limitation in expressiveness: in [lock], we
are creating a resource [t option] and so to make use of the critical
section we use a non-obvious control flow:

  try
    Some (new t (incr_if_nonzero (); p))
  with
  | Exit -> None

To avoid this style, [new] could take inspiration from typeclasses and
allow the creation of compound resources:

  new (t option) (if incr_if_nonzero () then Some p else None)


We also noticed that for unsafe block it may be necessary to introduce
a critical section by hand (see FIXME comment).


* Type inference vs. accessing the contents of a resource with
  custom destructor

let make_weak p = p

   int Atomic.t * ^a &@global const ->> int Atomic.t * ^a &@global const
   (int Atomic.t * ^a &@global const) const ->> int Atomic.t * ^a &@global const
   t & const -@>> int Atomic.t * ^a &@global const

let get (_, x) = x

   int Atomic.t * ^a &@global const ->> ^a &@global const
   ^a t & -@>> ^a &@global const
   ^a t & -@>> ^a & const

These chains of type specialisation are not clear yet; note that one
has to add an access to the list of effects. On the other hand, if we
have instead explicit operations to access types with custom
destructors:

   access : (^a with destructor ..) &@r -[@r]>> ^a &@r
   access_const : (^a with destructor ..) &@r const -[@r]>> ^a &@r const

(notation not definitive), then it becomes more straightforward.

let make_weak = access_const

   (^a with destructor ..) &@r const -[@r]> ^a &@r const
   [^a := int Atomic.t * ^a &@global const]
   ((int Atomic.t * ^a &@global const) with destructor ..) &@r const -[@r]> int Atomic.t * ^a &@global const
   [(int Atomic.t * ^a &@global const) with destructor .. := t]
   [int Atomic.t * ^a &@global const := weak]
   t &@r const -[@r]> weak

In fact, everything happens as if the new and the access are inverse
from each other in the constructor/destructor sense, and to guide type
inference it is more convenient to have a named constructor.

   type ^a t = Rc of ^a weak with destructor drop

e.g.

  let make *x = new Rc (Atomic.make 1, unsafe begin LObj.leak x end)

(keeping [new] because this is a fancy constructor that declares a
critical section for async exceptions.)

  let make_weak (Rc p) = p
  val make_weak : ^a t & const -@>> ^a weak

  let f &(Rc p) = p
  val f : ^a t & -@>> ^a weak

  let f *(Rc p) = p
  Error: taking ownership of the contents of a resource with custom
  destructor is forbidden

(The memory representation developed in the proposal is consistent
with this approach provided that the constructor is unboxed, as
already in place for single constructors in OCaml
<https://github.com/ocaml/ocaml/pull/606>.

On the other hand, we give up unraveling several layers of destructors
at once compared to the proposal.)
