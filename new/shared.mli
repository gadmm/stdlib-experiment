type ?+^a t : Copy
type ?+^a lock : Own

(* Better than Rc pointer. Limitation: the extent of what can be
   accessed from the destrutor. *)

val make : ^a ->> ^a t
(** [finalise x] gives ownership of x to the Gc; it is destroyed
    whenever the Gc likes (after there are no more copies around,
    including ones inside locks). *)

val lock : ^a t ->> ^a lock

val get : ^a lock as ^b ->> ^a &<^b>
