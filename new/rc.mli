(** Thread-safe reference-counting pointers.

   This module implements atomic RC pointers and weak pointers. The
   reference count is decreased atomically when the pointer is
   dropped, and increased atomically when explicitly copied. Weak
   pointers are garbage-collected values, so they can be copied
   without restriction.

   Atomic RC pointers can be used to share non-copiable resources
   between threads.
 *)

type ?+^a t : Own
(** The type of RC pointers containing a resource of type ^a. *)

type ?+^a weak
(** The type of weak pointers possibly containing a resource of type
   ^a *)

val make : ^a ->> ^a t
(** [make x] takes possession of [x] and returns a RC pointer holding
   it. *)

val copy : ^a t & const -@>> ^a t
(** [copy p] creates a new copy of [p] and increases the reference
   count. *)

val make_weak : ^a t & const -@>> ^a weak
(** [make_weak p] creates a weak pointer *)

val lock : ^a weak -@>> ^a t option
(** [lock w] returns [Some p] where [p] is an RC pointer associated to
   [w] if the resource is still live, or [None] if the reference count
   has reached zero. *)

val get : ^a t & const -@>> ^a & const
(** [get p] returns a const borrow of the enclosed resource. *)

val unique : ^a t ->> (^a, ^a t) result
(** [unique p] returns the enclosed resource if the reference count is
   1, or the original pointer otherwise. *)
