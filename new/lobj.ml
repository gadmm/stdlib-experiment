let magic *x = Obj.magic x
let leak *x = magic x
let take x = magic x
let unconstify x = magic x
