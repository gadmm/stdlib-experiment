(* Language design permitting interrupt-safe resources.
   October 2021.

   3 examples:
   - global account, reserve funds & commit
   - global list of in-progress and interrupted tasks
   - a file resource (interfacing FFI and resource acquisition)

   The challenge posed is handling the critical section between
   resource acquisition and initialisation in the presence of async
   exceptions (for construction), and the symmetrical critical section
   for deconstruction (i.e. pattern-matching on the owned resource,
   which disarms the destructor).

   Here is a natural solution based on having first-class resources
   (types with destructor) with:
   - a move operation that is atomic for interrupts;
   - a [begin[@explicit_polling] ... end] block that turns off
     dynamically the implicit polling of all exception-raising
     callbacks. Note: explicit polling is permitted (if an interrupt
     arises, then exception-safety can be reasoned about like with
     synchronous exceptions), and asynchronous callbacks that are
     guaranteed to never raise any exception are allowed;
   - runtime/FFI functions are based on explicit polling, e.g. they
     explicitly poll when releasing the runtime lock (or at least they
     are adapted to provide control on explicit polling).

   Notation: Const* vs. Const& to distinguish explicitly
   constructing/deconstructing from simply passing by value
   (borrowing)
*)

module Account : sig
  (* an account with ("pessimistic") transactions that revert the
     funds on error during processing *)
  type t
  val create : int -> t
  (* create account with this balance *)
  val balance : t -> int
  (* check current balance (statisfies >= 0 always) *)
  type transaction : Owned
  (* type of transactions to reserve some amount and confirm if
     processing succeeds (returns none if balance is insufficient). *)
  val reserve : t -> amount:int -> transaction option
  (* temporarily reserve funds from an account *)
  val perform : transaction -> unit
  (* perform the transaction (i.e. permanently remove the amount), on
     success paths *)
end = struct
  type t = int Atomic.t
  let create = Atomic.make
  let balance = Atomic.get
  type transaction = Abort of unit -> unit with destructor (fun f -> f ())
  let reserve acct ~amount =
    (* we actually need a CAS, which requires a slightly more verbose
       code, but the next example will already show such
       interrupt-safe CAS. *)
    let (old, *transaction) =
      begin[@explicit_polling]
        let old = Atomic.fetch_and_add acct (-amount) in
        old, Abort* (fun () -> Atomic.fetch_and_add acct amount)
      end
    in
    if old - amount >= 0 then Some transaction
    else None (* drop the transaction *)
    (* alternative: raise an exception on failure (but it's good
       that raising is not the only way to fail construction). *)
  let perform (Abort* _) = () (* disarm destructor *)
end

module StatusSet : sig
  (* A set of task statuses
     - a task status can be created
     - removable on completion ("optimistic" transaction)
     - updatable
     - status remains in the set and marked not in progress if handle
       dropped without some call to complete in the success path
     - global state is interrupt-safe
  *)
  type task : Owned
  type status = { completion : int ; in_progress : bool }
  val create : unit -> task
  val update : task & -> completion:int -> unit
  val complete : task -> unit
  val current_statuses : unit -> status list
end = struct
  (* helpers *)
  let rec atomic_map f a =
    let old = Atomic.get a in
    let next = f old in
    if not (Atomic.compare_and_set a old next) then atomic_map f a

  (* Same, but with a cas loop which is also atomic for polling, and
     with the non-polling section as small as possible. Replaces the
     current value [x] of [a] with [f x] atomically, and returns the
     result of appying [g] to the new value of [x]. There is no
     implicit polling between the moment [a] is successfully updated
     and [g] returns. *)
  let atomic_map_masked f *g a =
    let try_set () =
      let old = Atomic.get a in
      let next = f old in
      begin[@explicit_polling]
        if (Atomic.compare_and_set a old next) then Some (g next)
        else None
      end
    in
    let rec atomic_map_masked () =
      (* Note moving of resources *)
      match try_set () with
      | Some *res -> res
      | None -> atomic_map_masked ()
    in
    atomic_map_masked ()

  type status = { completion : int ; in_progress : bool }

  module StatusSet = Set.Make (
  struct
    type t = status Atomic.t
    val compare = (==)
  end)

  let global_set = Atomic.make StatusSet.empty

  let current_statuses () =
    let seq = StatusSet.to_seq (Atomic.get global_set) in
    let statuses = Seq.map Atomic.get seq in
    List.of_seq statuses

  let drop =
    atomic_map (fun { completion ; _ } -> { completion ; in_progress = false })

  type task = Task of status Atomic.t with destructor drop

  let create () =
    let t = Atomic.make { completion = 0 ; in_progress = true } in
    let add = StatusSet.add task in
    let acquire _ = Task* t in
    atomic_map_masked add acquire global_set

  let update (Task& t) ~completion =
    (* not sure about the notation Task&, but we need something *)
    atomic_map (fun { in_progress ; _ } -> { completion ; in_progress }) t

  let complete *task =
    let Task& t = &task in
    let remove = StatusSet.remove t in
    let acquire _ =
      (* disarm destructor *)
      let Task* _ = task in ()
    in
    (* Note: [acquire] owns [task] at this point *)
    atomic_map_masked remove acquire global_set
end

module File_out : sig
  (* Here the challenge is to create an interrupt-safe file resource
     using the OCaml primitives. *)
  type t : Owned
  val open_text : string -> t
  val length : t & -> int64
  val close : t -> unit (* close but do not ignore errors *)
  val output_string : t & -> unit
end = struct
  (* close_noerr should never poll for interrupts *)
  type t = File of in_channel with destructor Out_channel.close_noerr

  let open_text name =
    (* should we consider outputting a warning if a constructor is not
       within a no_interrupt block? *)
    begin[@explicit_polling]
      File* (Out_channel.open_text name)
      (* Note: the correct polling behaviour is to poll when releasing
         the runtime lock. We assume that Out_channel.open_text does it
         by following the *explicit polling* semantics. But then we
         need to make a special case of system calls in destructors,
         which should never poll (e.g. change the FFI using extra
         polling option or directly returning a resource).
         Another option is EAGAIN-style semantics. *)
    end

  let length (&File chan) = Out_channel.length chan

  external close_out_channel : out_channel -> unit = "caml_ml_close_channel"

  (* reimplementing Out_channel.close (style dictates that the no-poll
     block is as small as possible, in order to be as explicit as
     possible about the critical section we reason about). *)
  let close *f =
    Out_channel.flush &f ;
    begin[@explicit_polling]
      (* disarm destructor *)
      let *File chan = f in
      (* explicit polling is allowed inside close_out_channel *)
      close_out_channel chan
    end
end
