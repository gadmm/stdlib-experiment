type ?+^a shared = Finalised of ^a &@global
type ?+^a lock = t shared with destructor (fun _ -> ())

let destroy_finalised x = drop (take x)

let make x =
  let res = Finalised (leak x) in
  Gc.finalise destroy_finalised res;
  res

let get (x : ^a lock as ^b) : ^a &@<^b> =
  let Finalised y = x in y
