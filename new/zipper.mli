(** A zipper of lists

  ^a : Copy => usual GC'd zipper with sharing.
  ^a : Own => a zipper of owned resources. No alloc on traversal.
  ^a : Copy@ or Seq@ => a zipper that traverses borrowed data, and
       allocates new cells with the GC.

  Example: borrow a list of resources and explore it with a zipper.

    val l : ^a list
    Zipper.from_list &l : ^a & Zipper.t (= ^a Zipper.t &)

  Example: borrow the value under the cursor in an owner zipper.

    val z : ^a Zipper.t
    current &z : ^a &

 *)

type +^a t
(* implies:
     <^a t> = <^a>
     ^a t & = ^a & t
     ^a t const = ^a const t
 *)

val from_list : ^a list ->> ^a t
val add : ^a ->> ^a t -@>> ^a t
val right : ^a t -@>> ^a t
val has_right : ^a t & const -@>> bool
val left : ^a t -@>> ^a t
val has_left : ^a t & const -@>> bool
val current : ^a t -@>> ^a
val previous : ^a t -@>> ^a
