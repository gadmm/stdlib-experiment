(** This is Huet's original Zipper. We give it a resource-polymorphic
   interface:

    Examples:

      * Borrow a list of resources and explore it

          val l : t tree
          from_tree &l : t & location

      * Borrow the value under the cursor in an owner zipper

          val z : t location
          current &z : t &

   Note: moving in an owned Huet zipper changes the order of
   destruction of the contents.

 *)
type ^a tree = Item of ^a | Section of ^a tree list
type +^a location
val from_tree : ^a tree ->> ^a location
val current : ^a location -@>> ^a tree
val go_left : ^a location -@>> ^a location
val go_right : ^a location -@>> ^a location
val go_up : ^a location -@>> ^a location
val go_down : ^a location -@>> ^a location
val change : ^a location ->> ^a tree -@>> ^a location
val insert_right : ^a location ->> ^a tree -@>> ^a location
val insert_left : ^a location ->> ^a tree -@>> ^a location
val insert_down : ^a location ->> ^a tree -@>> ^a location
val delete : ^a location -@>> ^a location
