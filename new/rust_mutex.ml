type ^a t = Mutex.t * ^a

type guard = Mutex.t with destructor Mutex.unlock

type (^a, @r) lock = guard * ^a &@r const
(* Note: nothing to root in this special case; note that the GC->RAII
   coercions must first test whether the pointer is in the GC heap so
   if the pointer associated to [^a & const] is in the linear heap
   (i.e. immediately borrowed) then there is no cost of rooting. *)

let create *value = (Mutex.create (), value)

let lock (m, x) = (new guard (Mutex.lock m; m), x)

let try_lock (m, x) =
  try
    Some (new guard (if Mutex.try_lock m then m else raise Exit), x)
  with
  | Exit -> None

let get (_, x) = unsafe begin LObj.unconstify x end

let get_const = snd

let consume = snd
