(** A container for a single value protected by a mutex inspired by
   Rust's std::sync::Mutex. *)

type ?+^a t : <^a>
(** the type of mutexed containers *)

type (?+^a, @r) lock : Own
(** the type of guards *)

val create : ^a ->> ^a t
(** [create x] takes possession of the argument and protects it with a
   mutex. *)

val lock : ^a t & const -@>> (^a, @) lock
(** [lock m] locks the mutex and returns an RAII guard from which the
   value can be accessed. *)

val try_lock : ^a t & const -@>> (^a, @) lock option
(** [try_lock m] is the same as lock but it does not block if the
   mutex cannot be immediately acquired and returns None instead. *)

val get : (^a, @r) lock &@s -@>> ^a &@(r,s)
(** [get l] returns a borrowed pointer to the value protected by the
   mutex. *)

val get_const : (^a, @r) lock &@s const -@>> ^a &@(r,s) const
(** [get_const l] returns a copiable borrowed pointer to the value
   protected by the mutex. *)

val consume : ^a t ->> ^a
(** [consume m] gives back ownership of the value protected by the mutex. *)
