val magic : ^a -[unsafe]>> ^b

val leak : ^a -[unsafe]>> ^a &@global
(** [leak x] leaks [x] by never calling the destructor and returning a
   global reference to it. *)

val take : ^a & const -[unsafe]>> ^a
(** [take x] turns a borrowed value into an owned value. *)

val unconstify : ^a const -[unsafe]>> ^a
(** remove const. *)
