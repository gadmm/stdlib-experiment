module type OrderedType =
  sig
    type t
    val compare: t & const ->> t & const -@>> int
  end

module type Singleton =
  sig
    type elt : Own
    type t : <elt>
    val singleton: elt -@>> t
    val get: t -@>> elt
  end

type 'elt box = unit * 'elt

module MakeImpl (Ord : OrderedType) =
  struct
    let singleton x =
      if Int_compare.(0 = Ord.compare const&x const&x)
      then ((), x)
      else invalid_arg "Set.singleton"

    let get ((), x) = x
  end

module type PolySingleton =
  sig
    include Singleton
    module Borrow : Singleton with type elt = elt & and type t = t &
    module Const_borrow : Singleton with type elt = elt & const and type t = t & const
  end

module Make (Ord : OrderedType) : PolySingleton with type elt = Ord.t =
  struct
    module Impl = MakeImpl(Ord)

    include Impl
    type elt = Ord.t
    type t = elt box

    module Borrow =
      struct
        include Impl
        type elt = elt &
        type t = t &
      end

    module Const_borrow =
      struct
        include Impl
        type elt = elt & const
        type t = t & const
      end
  end
